object TemperatureScale extends Convert{
  val Celsius, Kelvin = Value
}

object TemperatureConvert {
	  def kelvinToCelsius(d: Double) = round(d - 273.15)

class ConvertWord

case class Temperature(degrees: Double, scale: TemperatureScale.Value) {
  import TemperatureScale._
  import TemperatureConvert._

    def apply(ignore: ConvertWord) = new {
    def celsius = toCelsius
    def kelvin = toKelvin
  }

    def to(newScale: TemperatureScale.Value) = newScale match {
    case Celsius => toCelsius
    case Kelvin => toKelvin
  }

  def toKelvin = scale match {
    case Celsius => Temperature(celsiusToKelvin(degrees), Kelvin)
    case Kelvin => this
  }

  class TemperatureBuilder(degrees: Double) {
  import TemperatureScale._
  def celsius = Temperature(degrees, Celsius)
  def kelvin = Temperature(degrees, Kelvin)
}

package object temperature {
  val to = new ConvertWord
  val celsius = TemperatureScale.Celsius
  val kelvin = TemperatureScale.Kelvin
}
