Médérick Plançon

Conversion K °C udf 
---------

`val udfTemp = udf((s: Double) => (s - 273.15).toInt )`

Compiling
---------

The project can be compiled with the standard command, `mvn compile`

Running
-------
To run the application, type `mvn scala:run`

To run unit-tests, type `mvn test`



